package com.algaworks.api.api.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.algaworks.api.domain.model.Cliente;

@RestController
public class ClienteController {
	
	@GetMapping("/clientes")
	public List<Cliente> listar() {
		var cliente1 = new Cliente();
		cliente1.setId(1L);
		cliente1.setNome("João das Couves");
		cliente1.setTelefone("34 99999-1111");
		cliente1.setEmail("joaodascouves@algaworks.com");
		
		var cliente2 = new Cliente();
		cliente2.setId(1L);
		cliente2.setNome("Rafael");
		cliente2.setTelefone("55 99171-1114");
		cliente2.setEmail("rafaelmartins@algaworks.com");
		
		var cliente3 = new Cliente();
		cliente3.setId(1L);
		cliente3.setNome("Medusa");
		cliente3.setTelefone("55 44214-7484");
		cliente3.setEmail("mdusa@algaworks.com");
		
		return Arrays.asList(cliente1, cliente2, cliente3);
	}
}
